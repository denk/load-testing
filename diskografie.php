  <?php include 'header.php'; ?>
   <h1>Diskografie</h1>
  
   <table class="diskografie" border="0" height="370">
   <tr><th width="50">Rok</th><th width="280">Album</th>				<th width="280">Vydavatel</th></tr>
   <tr><td>1993</td><td>Doggystyle</td>									<td>Death Row</td></tr>
   <tr><td>1996</td><td>Tha Doggfather</td>								<td>Death Row</td></tr>
   <tr><td>1998</td><td>Da Game Is to Be Sold, Not to Be Told</td>		<td>No limit</td></tr>
   <tr><td>1999</td><td>No Limit Top Dogg</td>							<td>No limit</td></tr>
   <tr><td>2000</td><td>Tha Last Meal</td>								<td>No limit</td></tr>
   <tr><td>2002</td><td>Paid tha Cost to Be da Boss</td>				<td>Priority, Capitol</td></tr>
   <tr><td>2004</td><td>R&G (Rhythm & Gangsta): The Masterpiece</td>	<td>Doggystyle, Star Trak, Geffen</td></tr>
   <tr><td>2006</td><td>Tha Blue Carpet Treatment</td>					<td>Doggystyle, Geffen</td></tr>
   <tr><td>2008</td><td>Ego Trippin'</td>								<td>Doggystyle, Geffen</td></tr>
   <tr><td>2009</td><td>Malice N Wonderland</td>						<td>Doggystyle, Priority</td></tr>
   <tr><td>2011</td><td>Doggumentary</td>								<td>Doggystyle, Priority, Capitol, EMI</td></tr>
   </table>
   
  <?php include 'footer.php'; ?>