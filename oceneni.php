  <?php include 'header.php'; ?>
   <h1>
Ocen�n�   
   </h1>
   <p class="textobsah">
Snoop Dogg byl nominov�n na cenu Grammy 13x, ale vyhr�l jen jednou.<br>
Zde je p�ehled jeho nominac�:
  </p>
   <table class="diskografie" border="0" height="500">
   <tr><th width="50">Rok</th><th width="280">Kategorie</th>					<th width="280">N�zev p�sn�</th></tr>
   <tr><td>1995</td><td>Best Rap Performance by a Duo or Group
(with Dr. Dre)</td>																<td>"Nuthin' But a "G" Thang"</td></tr>
   <tr><td>1996</td><td>Best Rap Solo Performance</td>							<td>"Gin and Juice"</td></tr>
   <tr><td>1996</td><td>Best Rap Performance by a Duo or Group</td>				<td>"What Would You Do"</td></tr>
   <tr><td>2000</td><td>Best Rap Performance by a Duo or Group
(with Dr. Dre)</td>																<td>"Still D.R.E."</td></tr>
   <tr><td>2001</td><td>Best Rap Performance by a Duo or Group
(with Dr. Dre)</td>																<td>"The Next Episode"</td></tr>
   <tr><td>2003</td><td>Best Rap/Sung Collaboration
(with Pharrell and Charlie Wilson)</td>											<td>"Beautiful"</td></tr>
   <tr><td>2003</td><td>Best Rap Song
(with Chad Hugo and Pharrell Williams)</td>										<td>"Beautiful"</td></tr>
   <tr><td>2004</td><td>Best Rap Performance by a Duo or Group
(with Pharrell Williams)</td>													<td>"Drop It Like It's Hot"</td></tr>
   <tr><td>2004</td><td>Best Rap Song
(with Pharrell Williams)</td>													<td>"Drop It Like It's Hot"</td></tr>
   <tr><td>2006</td><td>Best Rap/Sung Collaboration
(with Akon)</td>																<td>"I Wanna Love You"</td></tr>
   <tr><td>2009</td><td>Best Rap Song</td>										<td>"Sexual Eruption"</td></tr>
   <tr><td>2009</td><td>Best Rap Solo Performance</td>							<td>"Sexual Eruption"</td></tr>
   <tr><td>2011</td><td>Best Pop Collaboration With Vocals</td>					<td>"California Gurls" (Katy Perry feat. Snoop Dogg)</td></tr>
   </table>
    
   <p class="textobsah">
   Tak� vyhr�l �adu dal��ch ocen�n� (v�echny by se tady opravdu nevlezly):
   </p>
      <table class="diskografie" border="0" height="370">
   <tr><th width="50">Rok</th><th width="280">Kategorie</th>				<th width="280">Za co</th></tr>
   <tr><td>2003</td><td>BET Awards</td>														<td>Best Collaboration (with Pharrell)</td></tr>
   <tr><td>2004</td><td>Pretender Awards</td>												<td>Snitch of the Year</td></tr>
   <tr><td>2004</td><td>Adult Video News awards</td>										<td>Top Selling Tape of 2003 ("Snoop Dogg's Hustlaz: Diary of a Pimp")</td></tr>
   <tr><td>2005</td><td>MOBO Awards</td>													<td>Best video ("Drop it Like It's Hot")</td></tr>
   <tr><td>2005</td><td>MTV European Music Awards</td>										<td>Best Hip-Hop Artist</td></tr>
   <tr><td>2005</td><td>The Billboard R&B/Hip-Hop Conference Awards </td>					<td>Hot Rap Track ("Drop it Like It's Hot")</td></tr>
   <tr><td>2006</td><td>MTV Australia Video Music Awards</td>								<td>Best hip hop video ("Drop it Like It's Hot")</td></tr>
   <tr><td>2006</td><td>Los Angeles Chapter Recording Academy Honors</td>					<td>Recipient (Snoop Dogg)</td></tr>
   <tr><td>2006</td><td>MTV Video Music Awards</td>											<td>Best Dance Video ("Buttons" with The Pussycat Dolls)</td></tr>
   <tr><td>2007</td><td>MTV Australia Video Music Awards</td>								<td>Best Hip Hop Video ("That's That")</td></tr>
   <tr><td>2010</td><td>MTV Europe Music Awards 2010</td>									<td>Best Video (Katy Perry "California Gurls" featuring Snoop Dogg)</td></tr>
   </table>
    <?php include 'footer.php'; ?>